import bcrypt
import os

def load_encrypted_password(file_name):
    with open(file_name, 'rb') as file:
        return file.read()

def check_password(encrypted_password_file_name,user_password_file_name):
    file_name = encrypted_password_file_name
    if not os.path.exists(file_name):
        print(f"Arquivo '{file_name}' não encontrado.")
        return

    existing_encrypted_password = load_encrypted_password(file_name)

    user_password_file_name = user_password_file_name
    if not os.path.exists(user_password_file_name):
        print(f"Arquivo '{user_password_file_name}' não encontrado.")
        return

    with open(user_password_file_name, 'rb') as user_password_file:
        user_password = user_password_file.read().strip()

    print(f"Encrypted Password: {existing_encrypted_password}")
    print(f"User Password: {user_password}")

    is_valid_password = bcrypt.checkpw(user_password, existing_encrypted_password)

    if is_valid_password:
        return "Correct password"
    else:
        return "Incorrect password"


check_password('encrypted_password.txt','user_password.txt')
