import bcrypt
import random
import string
import os



def generate_random_password(size=12):
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for i in range(size))
    return password

def encrypt_password(password):
    # Geração do salt
    salt = bcrypt.gensalt()
    
    # Criptografar a senha
    password_bytes = password.encode('utf-8')
    encrypted_password = bcrypt.hashpw(password_bytes, salt)
    
    return encrypted_password

# Gerar uma senha aleatória
random_password = generate_random_password()
print("Senha aleatória gerada:", random_password)

# Encriptar a senha
encrypted_password = encrypt_password(random_password)
print("Senha encriptada:", encrypted_password)


# Verificar se o arquivo já existe
file_name = "encrypted_password.txt"
if not os.path.exists(file_name):
    # Se o arquivo não existe, cria o arquivo vazio
    open(file_name, 'a').close()

# Salvar a senha encriptada em um arquivo
with open(file_name, 'wb') as file:
    file.write(encrypted_password)
    print(f"Senha encriptada salva no arquivo '{file_name}'")
