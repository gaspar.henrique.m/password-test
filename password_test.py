import os
import bcrypt
from main import load_encrypted_password, check_password


def test_load_encrypted_password():
    
    test_password = b'testpassword'
    test_file_name = 'test_encrypted_password.txt'

    with open(test_file_name, 'wb') as file:
        file.write(bcrypt.hashpw(test_password, bcrypt.gensalt()))

    loaded_password = load_encrypted_password(test_file_name)
    assert bcrypt.checkpw(test_password, loaded_password)
    os.remove(test_file_name)  

def test_check_password():
    test_password = b'testpassword'
    test_file_name = 'test_user_password.txt'

    with open(test_file_name, 'wb') as file:
        file.write(test_password)

    encrypted_password_file = 'test_encrypted_password.txt'
    with open(encrypted_password_file, 'wb') as file:
        file.write(bcrypt.hashpw(test_password, bcrypt.gensalt()))

    assert  check_password('test_encrypted_password.txt','test_user_password.txt') == "Correct password"

    print("Correct password")
    
    os.remove(test_file_name)  
    os.remove(encrypted_password_file)

